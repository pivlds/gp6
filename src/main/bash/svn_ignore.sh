#!/bin/bash

# Configures the project root to take ignore git and maven directories in svn

# @param $1 the path from the svn root if the project is committed nested.

cd `svn info | grep 'Working Copy Root Path:' | awk '{print $NF}'`/$1

echo WORKING DIR: `pwd`

echo SETTING INGORES FROM .svnignore

svn propset svn:ignore -F .svnignore .

echo CHECKING IGNORES FROM svn:ignore

svn propget -R svn:ignore .

echo CHECKING REPO STATUS

svn status --depth=infinity --no-ignore .



package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class TestSelectors implements Serializable {

	/**
	 * DEFAULT serialization id
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	HashMap<String, TestSelector> selectors = new HashMap<>();

	public TestSelector get(String name) {
		if (name != null)
			name.trim();

		if (!selectors.containsKey(name)) {
			TestSelector r = new TestSelector();
			r.defaultInit();
			selectors.put(name, r);
		}

		return selectors.get(name);
	}

	public String getTestString() {
		return "test string";
	}

}

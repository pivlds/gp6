package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.reports.EnrollmentVelocityReport;
import edu.jmu.cs474.pivlds.reports.ReportTestHistory;
import edu.jmu.cs474.pivlds.reports.ReportVelocityTrends;

/**
 * This multiselector ensures that multiple reports on the same page will not
 * interfere with caching facility, which relies on consistency of parameters
 * between run attempts..
 * 
 * @author james
 *
 */
@Named
@SessionScoped
public class Reports implements Serializable {

	HashMap<String, EnrollmentVelocityReport> evs = new HashMap<>();
	HashMap<String, ReportVelocityTrends> vts = new HashMap<>();
	HashMap<String, ReportTestHistory> ths = new HashMap<>();

	public EnrollmentVelocityReport ev(String key) {
		if (!evs.containsKey(key)) {
			evs.put(key, new EnrollmentVelocityReport());
		}
		evs.get(key).setId(key);
		return evs.get(key);

	}

	public ReportVelocityTrends vt(String key) {
		if (!vts.containsKey(key)) {
			vts.put(key, new ReportVelocityTrends());
		}
		vts.get(key).setId(key);
		return vts.get(key);
	}

	public ReportTestHistory th(String key) {
		if (!ths.containsKey(key)) {
			ths.put(key, new ReportTestHistory());
		}
		ths.get(key).setId(key);
		return ths.get(key);
	}

}

package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class TagSelectors implements Serializable {

	/**
	 * DEFAULT serialization id
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	HashMap<String, TagSelector> selectors = new HashMap<>();

	public TagSelector get(String name) {
		if (name != null)
			name.trim();

		if (!selectors.containsKey(name)) {
			TagSelector r = new TagSelector();
			selectors.put(name, r);
		}

		return selectors.get(name);
	}

}

package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class SelectorKeys implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String schoolSelectorKey;

	public String getSchool() {
		return schoolSelectorKey;
	}

	public void setSchool(String key) {
		this.schoolSelectorKey = key;
	}

}

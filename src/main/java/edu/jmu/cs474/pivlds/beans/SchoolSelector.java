package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.School;

/**
 * Session level bean for selecting a school instance.
 * 
 * @author james
 *
 */
@Named
@SessionScoped
public class SchoolSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String like_text;

	School current;

	List<School> searchResults = new LinkedList<>();

	public School getCurrent() {
		if (current == null)
			selectEntireState();
		return current;
	}

	public String getLikeText() {
		return like_text;
	}

	public void setLikeText(String pattern) {
		like_text = pattern;
	}

	public void doLike() {
		selectLike(like_text);
	}

	public void selectLike(String pattern) {
		try {
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement("Select * from school_like(?) NATURAL JOIN division");) {
				ps.setString(1, pattern);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next())
						current.read(rs);
					else
						current.reset();
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public void doKey() {
		selectKey(like_text);
	}

	public void selectEntireDivision() {
		try (Connection c = PivldsDb.getConnection();
				PreparedStatement ps = c.prepareStatement("SELECT * FROM entire_division(?) NATURAL JOIN division");) {
			ps.setInt(1, current.getDiv_num());
			try (ResultSet rs = ps.executeQuery();) {
				if (!rs.next()) {
					PageMessage.message("No Division Information Available");
				} else {
					School s = new School();
					s.read(rs);
					current = s;
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public void selectEntireState() {
		try (Connection c = PivldsDb.getConnection();
				PreparedStatement ps = c.prepareStatement("SELECT * FROM entire_state() NATURAL JOIN division");
				ResultSet rs = ps.executeQuery();) {
			if (!rs.next()) {
				PageMessage.message("No Results");
			} else {
				School s = new School();
				s.read(rs);
				current = s;
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public void selectKey(String pattern) {
		try {
			pattern = pattern.trim();
			if (!Pattern.matches("\\d+", pattern))
				throw new Exception("[NOT NUMERIC] " + pattern);

			int key = Integer.valueOf(pattern);
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement("Select * from school(?) NATURAL JOIN division");) {
				ps.setInt(1, key);
				try (ResultSet rs = ps.executeQuery();) {
					rs.next();
					current.read(rs);
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	String searchText;

	public String getSearch() {
		return searchText;
	}

	public void setSearch(String text) {
		searchResults.clear();
		try {
			searchText = text.trim();
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c
							.prepareStatement("SELECT * FROM school_search(?) NATURAL JOIN division");) {
				ps.setString(1, text);
				try (ResultSet rs = ps.executeQuery();) {
					if (!rs.next()) {
						PageMessage.message("No Results");
					} else {
						do {
							School s = new School();
							s.read(rs);
							searchResults.add(s);
						} while (rs.next());
						PageMessage.message("Results: " + searchResults.size());
					}
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public List<School> getSearchResults() {
		return searchResults;
	}

	School selectedSchool;

	public void setSelectedSchool(School school) {
		if (school != null)
			current = school;
		else
			current.reset();
	}

	public void reset(Exception e) {
		if (current != null)
			current.reset();
		PageMessage.message(e);
	}

}

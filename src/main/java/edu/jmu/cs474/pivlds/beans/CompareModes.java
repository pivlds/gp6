package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class CompareModes implements Serializable {

	public enum ReportMode {
		Enrollment_History, Velocity_Trends, TAG_History;

		public String prettyString() {
			return toString().replaceAll("\\_", " ");
		}
	}

	HashMap<String, Selector> modes = new HashMap<>();

	public Selector selector(String key) {
		Selector r = modes.get(key);
		if (r == null) {
			r = new Selector();
			modes.put(key, r);
		}
		return r;
	}

	public ReportMode[] getModes() {
		return ReportMode.values();
	}

	public static class Selector implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		ReportMode mode;

		public Selector() {
			this.mode = ReportMode.Enrollment_History;
		}

		public ReportMode getMode() {
			return mode;
		}

		public void setMode(ReportMode mode) {
			this.mode = mode;
		}
	}

}

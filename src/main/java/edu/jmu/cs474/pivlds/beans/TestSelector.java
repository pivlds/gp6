package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.TestId;

/**
 * Session level bean for selecting {@link TestId}
 * 
 * @author James Arlow
 *
 */
@Named
@SessionScoped
public class TestSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static List<TestId> cache;

	public TestId[] getValues() {
		if (cache == null) {
			cache = new LinkedList<TestId>();

			String sql = //
					"SELECT * " + //
							"FROM test_id " + //
							"NATURAL JOIN test_category " + //
							"NATURAL JOIN test_level " + //
							"ORDER BY subject, section, tlvl_pk ";

			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement(sql);
					ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					TestId test = new TestId();
					test.read(rs);
					cache.add(test);
				}
				if (cache.size() == 0)
					throw new Exception("No results");
			} catch (Exception e) {
				PageMessage.message(e);
				e.printStackTrace();
				cache = null;
			}
		}

		return cache == null ? new TestId[0] : cache.toArray(new TestId[cache.size()]);
	}

	TestId selected = new TestId();

	public TestId getSelected() {
		return selected;
	}

	public void setSelected(TestId selected) {
		System.out.println("SET: " + selected);
		this.selected = selected;
	}

	public static TestId find(int pk) {
		for (TestId test : cache) {
			if (test.getTest_pk() == pk)
				return test;
		}
		return null;
	}

	@PostConstruct
	public void defaultInit() {
		TestId[] vals = getValues();
		selected = vals == null || vals.length == 0 ? null : getValues()[0];
	}

}

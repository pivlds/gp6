package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.model.Demographic;

/**
 * Session level JNDI bean for the demographic selector. Allows the template
 * pages to bind to the selected demographic value.
 * 
 * @author James 
 *
 */
@Named
@SessionScoped
public class DemographicSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Demographic current = new Demographic();

	public Demographic getCurrent() {
		return current;
	}

}

package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.model.Demographic;

@Named
@SessionScoped
public class DemographicSelectors implements Serializable {

	/**
	 * DEFAULT serialization id
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	HashMap<String, Demographic> selectors = new HashMap<>();

	public Demographic get(String name) {
		if (name != null)
			name.trim();

		if (!selectors.containsKey(name)) {
			selectors.put(name, new Demographic());
		}

		return selectors.get(name);
	}

}

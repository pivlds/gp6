package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class SchoolSelectors implements Serializable {

	/**
	 * DEFAULT serialization id
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	HashMap<String, SchoolSelector> selectors = new HashMap<>();

	public SchoolSelector get(String name) {
		if (name != null)
			name.trim();

		if (!selectors.containsKey(name)) {
			selectors.put(name, new SchoolSelector());
		}

		return selectors.get(name);
	}
	
	@Inject
	SchoolSelector _default;
	
	@PostConstruct
	public void initDefault() {
		selectors.put(null, _default);
	}

}

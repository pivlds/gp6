package edu.jmu.cs474.pivlds.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class HelpOptions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	boolean showExplanations = true;

	boolean showInstructions = true;

	public boolean isShowExplanations() {
		return showExplanations;
	}

	public void setShowExplanations(boolean showExplanations) {
		this.showExplanations = showExplanations;
	}

	public boolean isShowInstructions() {
		return showInstructions;
	}

	public void setShowInstructions(boolean showInstructions) {
		this.showInstructions = showInstructions;
	}

	public String toggleExplanations() {
		showExplanations = !showExplanations;
		return null;
	}

	public String toggleInstructions() {
		showInstructions = !showInstructions;
		return null;
	}

}

package edu.jmu.cs474.pivlds.beans;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.inject.Singleton;

import edu.jmu.cs474.pivlds.model.Demographic;
import edu.jmu.cs474.pivlds.model.TestAchievementGroup;

/**
 * Provides enum values for option-boxes/lists.
 * <p>
 * TODO: Migrate other enum option lists here.
 * 
 * @author James Arlow
 *
 */
@Named
@ApplicationScoped
public class SelectorGroups {

	public TestAchievementGroup[] getTags() {
		return TestAchievementGroup.values();
	}

	public Demographic.Disabil[] getDisabils() {
		return Demographic.Disabil.values();
	}

	public Demographic.Disadva[] getDisadvas() {
		return Demographic.Disadva.values();
	}

	public Demographic.Gender[] getGenders() {
		return Demographic.Gender.values();
	}

	public Demographic.LEP[] getLeps() {
		return Demographic.LEP.values();
	}

	public Demographic.Race[] getRaces() {
		return Demographic.Race.values();
	}

}

package edu.jmu.cs474.pivlds.reports;

import java.util.Properties;

import javax.inject.Inject;

import edu.jmu.cs474.pivlds.beans.DemographicSelector;
import edu.jmu.cs474.pivlds.beans.SchoolSelector;
import edu.jmu.cs474.pivlds.beans.TestSelector;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.Demographic;
import edu.jmu.cs474.pivlds.model.School;
import edu.jmu.cs474.pivlds.model.TestId;

/**
 * Common superclass for reports that target a specific test.
 * 
 * @author James Arlow
 *
 */
public abstract class TestReport extends CachedReport {

	@Inject
	SchoolSelector schSelect;

	@Inject
	DemographicSelector demSelect;

	@Inject
	TestSelector testSelect;

	Boolean ready;

	public boolean checkReady() {
		if (ready != null)
			return ready;

		if (getSchool() == null || !getSchool().isValid()) {
			PageMessage.message("Invalid School ");
			ready = false;
		} else
			ready = true;
		return ready;
	}

	protected School school;

	public School getSchool() {
		return school == null ? schSelect.getCurrent() : school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	protected Demographic demographic;

	public Demographic getDemographic() {
		return demographic == null ? demSelect.getCurrent() : demographic;
	}

	public void setDemographic(Demographic demographic) {
		this.demographic = demographic;
	}

	protected TestId test;

	public TestId getTest() {
		return test == null ? testSelect.getSelected() : test;
	}

	public void setTest(TestId test) {
		this.test = test;
	}

	@Override
	public void populateContext(Properties context) {
		context.setProperty("sch", "" + getSchool());
		context.setProperty("dem", "" + getDemographic());
		context.setProperty("test", "" + getTest());
	}

}

package edu.jmu.cs474.pivlds.reports;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.beans.DemographicSelector;
import edu.jmu.cs474.pivlds.beans.SchoolSelector;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.Demographic;
import edu.jmu.cs474.pivlds.model.School;

/**
 * View level bean for calling `report_enrollment_velocity(...)`.
 * 
 * @author james
 *
 */
@Named
@ViewScoped
public class EnrollmentVelocityReport extends CachedReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	SchoolSelector schSel;

	@Inject
	DemographicSelector demSel;

	Entry[] cache = new Entry[0];

	public Entry[] query() {
		runQuery();
		return cache;
	}

	public Entry[] query(School sch, Demographic dem) {
		setSchool(sch);
		setDemographic(dem);
		return query();
	}

	public static class Entry implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Integer sch_year;
		Integer fall_cnt;
		Integer all_cnt;
		Integer diff_cnt;
		Double dem_weight;
		Double growth;
		Double growth_weight;

		public Integer getSch_year() {
			return sch_year;
		}

		public void setSch_year(Integer sch_year) {
			this.sch_year = sch_year;
		}

		public Integer getFall_cnt() {
			return fall_cnt;
		}

		public void setFall_cnt(Integer fall_cnt) {
			this.fall_cnt = fall_cnt;
		}

		public Integer getAll_cnt() {
			return all_cnt;
		}

		public void setAll_cnt(Integer all_cnt) {
			this.all_cnt = all_cnt;
		}

		public Integer getDiff_cnt() {
			return diff_cnt;
		}

		public void setDiff_cnt(Integer diff) {
			this.diff_cnt = diff;
		}

		public Double getDem_weight() {
			return dem_weight;
		}

		public void setDem_weight(Double dem_weight) {
			this.dem_weight = dem_weight;
		}

		public Double getGrowth() {
			return growth;
		}

		public void setGrowth(Double growth) {
			this.growth = growth;
		}

		public Double getGrowth_weight() {
			return growth_weight;
		}

		public void setGrowth_weight(Double growth_weight) {
			this.growth_weight = growth_weight;
		}

		public void read(ResultSet rs) throws SQLException {
			sch_year = rs.getInt("sch_year");
			fall_cnt = rs.getInt("fall_cnt");
			all_cnt = rs.getInt("all_cnt");
			diff_cnt = rs.getInt("diff_cnt");
			dem_weight = rs.getDouble("dem_weight");
			growth = rs.getDouble("growth");
			growth_weight = rs.getDouble("growth_weight");
		}

	}

	@Override
	public void populateContext(Properties context) {
		context.setProperty("sch", "" + getSchool());
		context.setProperty("dem", "" + getDemographic());
	}

	@Override
	public void executeQuery() {
		School sch = getSchool();
		Demographic dem = getDemographic();

		String query = "Select * FROM report_enrollment_velocity(school(?),demographic(?))";

		List<Entry> entries = new LinkedList<Entry>();
		try (Connection c = PivldsDb.getConnection(); PreparedStatement ps = c.prepareStatement(query);) {
			ps.setInt(1, sch.getSch_pk());
			ps.setInt(2, dem.getKey());

			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					Entry e = new Entry();
					e.read(rs);
					entries.add(e);
				}
			}
		} catch (Exception e) {
			PageMessage.message(e);
		}
		cache = entries.toArray(new Entry[entries.size()]);
	}

	School school;

	public School getSchool() {
		return school == null ? schSel.getCurrent() : school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	Demographic demographic;

	public Demographic getDemographic() {
		return demographic;
	}

	public void setDemographic(Demographic demographic) {
		this.demographic = demographic;
	}

}

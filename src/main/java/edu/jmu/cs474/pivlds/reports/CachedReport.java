package edu.jmu.cs474.pivlds.reports;

import java.util.Properties;

public abstract class CachedReport {

	boolean dirty = false;
	private Properties context = new Properties();

	public boolean checkDirty(Properties props) {
		if (!context.equals(props))
			return true;

		for (Object key : context.keySet()) {
			if (!context.get(key).equals(props.get(key)))
				return true;
		}

		return false;
	}

	public Properties loadContext() {
		Properties props = new Properties();
		populateContext(props);
		return props;
	}

	public abstract void populateContext(Properties context);

	public void readContext() {
		String pre = "" + context;

		Properties props = loadContext();
		if (checkDirty(props)) {
			dirty = true;
			context = props;
		} else
			dirty = false;

		debug("dirty?: [" + dirty + "] " + getClass().getSimpleName() + (dirty ? " | " + pre + " => " + props : ""));
	}

	public void runQuery() {
		readContext();
		if (!dirty)
			return;
		debug("query: " + getClass().getSimpleName() + " | " + context);
		executeQuery();
	}

	public abstract void executeQuery();

	static boolean DEBUG = true;

	void debug(String message) {
		if (DEBUG) {
			System.out.print("[" + getReportId() + "]:");
			System.out.println(message);
		}
	}

	String id;

	public void setId(String id) {
		this.id = id;
	}

	String getReportId() {
		return getClass().getSimpleName() + (id == null ? "" : ":" + id);
	}

}

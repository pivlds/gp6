package edu.jmu.cs474.pivlds.reports;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.jmu.cs474.pivlds.PivldsDb;
import edu.jmu.cs474.pivlds.beans.TagSelector;
import edu.jmu.cs474.pivlds.jsf.PageMessage;
import edu.jmu.cs474.pivlds.model.Demographic;
import edu.jmu.cs474.pivlds.model.School;
import edu.jmu.cs474.pivlds.model.TestAchievementGroup;
import edu.jmu.cs474.pivlds.model.TestId;

/**
 * View level bean for calling `report_test_history(...)`.
 * 
 * @author James Arlow
 *
 */
@Named
@ViewScoped
public class ReportTestHistory extends TestReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data model for the postgres type returned by the report function.
	 * 
	 * @author James Arlow
	 *
	 */
	public static class Row implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		int year;
		float tag_rate;
		float tag_diff;
		float dem_weight;
		float rate_weighted;
		float diff_weighted;
		float velocity;
		float velocity_weighted;

		public void read(ResultSet rs) throws SQLException {
			year = rs.getInt("year");
			tag_rate = rs.getFloat("tag_rate");
			tag_diff = rs.getFloat("tag_diff");
			dem_weight = rs.getFloat("dem_weight");
			rate_weighted = rs.getFloat("rate_weighted");
			diff_weighted = rs.getFloat("diff_weighted");
			velocity = rs.getFloat("velocity");
			velocity_weighted = rs.getFloat("velocity_weighted");
		}

		public int getYear() {
			return year;
		}

		public float getTag_rate() {
			return tag_rate;
		}

		public float getTag_diff() {
			return tag_diff;
		}

		public float getDem_weight() {
			return dem_weight;
		}

		public float getRate_weighted() {
			return rate_weighted;
		}

		public float getDiff_weighted() {
			return diff_weighted;
		}

		public float getVelocity() {
			return velocity;
		}

		public float getVelocity_weighted() {
			return velocity_weighted;
		}

	}

	List<Row> results = new LinkedList<>();

	@Inject
	TagSelector tagSelect;

	public List<Row> run(TestAchievementGroup tag, TestId test, School sch, Demographic dem) {
		setTag(tag);
		setSchool(sch);
		setDemographic(dem);
		setTest(test);
		List<Row> r = getResults();
		return r;
	}

	public void executeQuery() {
		results.clear();
		try {
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement(
							"SELECT * FROM report_test_history(tag(?), test_id(?), school(?), demographic(?));");) {
				ps.setInt(1, getTag().tag_pk);
				ps.setInt(2, getTest().getTest_pk());
				ps.setInt(3, getSchool().getSch_pk());
				ps.setInt(4, getDemographic().getKey());
				try (ResultSet rs = ps.executeQuery();) {
					if (!rs.next()) {
						throw new Exception("No results [" + getClass().getSimpleName() + "]");
					} else {
						do {
							Row s = new Row();
							s.read(rs);
							results.add(s);
						} while (rs.next());
					}
				}
			}
		} catch (Exception e) {
			reset(e);
		}
	}

	public void reset(Exception e) {
		results.clear();
		if (e != null)
			PageMessage.message(e);
	}

	public List<Row> getResults() {
		if (checkReady()) {
			runQuery();
		}
		return results;
	}

	@Override
	public void populateContext(Properties p) {
		super.populateContext(p);
		p.put("tag", getTag());
	}

	protected TestAchievementGroup tag;

	public TestAchievementGroup getTag() {
		return tag == null ? tagSelect.getSelected() : tag;
	}

	public void setTag(TestAchievementGroup tag) {
		this.tag = tag;
	}

}

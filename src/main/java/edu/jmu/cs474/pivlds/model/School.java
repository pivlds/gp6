package edu.jmu.cs474.pivlds.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Data model for School table.
 * @author James Arlow
 *
 */
public class School implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Integer sch_pk;
	Integer sch_num;
	Integer div_num;
	String div_name;
	String sch_name;

	boolean valid = false;

	public Integer getSch_pk() {
		return sch_pk;
	}

	public Integer getSch_num() {
		return sch_num;
	}

	public Integer getDiv_num() {
		return div_num;
	}

	public String getDiv_name() {
		return div_name;
	}

	public String getSch_name() {
		return sch_name;
	}

	/**
	 * Used by JSF forms to check for invalid state.
	 * 
	 * @return true if this school contains valid db info.
	 */
	public boolean isValid() {
		return valid;
	}

	public void read(ResultSet rs) throws SQLException {
		sch_pk = rs.getInt("sch_pk");
		sch_num = rs.getInt("sch_num");
		div_num = rs.getInt("div_num");
		sch_name = rs.getString("sch_name");
		div_name = rs.getString("div_name");
		valid = true;
	}

	public void reset() {
		sch_pk = null;
		sch_num = null;
		div_num = null;
		sch_name = null;
		div_name = null;
		valid = false;
	}

	public String toString() {
		return div_name + " :: " + sch_name;
	}
	
}

package edu.jmu.cs474.pivlds.model;

import java.io.Serializable;

/**
 * Data model for `test_achievement_group`.
 * 
 * @author James Arlow
 *
 */
public enum TestAchievementGroup implements Serializable {

	LOW(0, "Fail"), MID(1, "Pass/Proficient"), TOP(2, "Pass/Advanced");

	final public int tag_pk;
	final public String sols;
	final public String name;

	TestAchievementGroup(int pk, String sols) {
		this.tag_pk = pk;
		this.sols = sols;
		this.name = toString();
	}
}

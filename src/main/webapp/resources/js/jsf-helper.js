
/**
 * Normalizes JSF ids so they can be fed to `document.querySelector`.
 * 
 * The `:` used to chain parents into the id needs to be escaped as `\:`
 * 
 * @param jsfId
 *            The JSF clientId to normalize.
 * 
 * @returns the normalized JSF clientId
 */
function jsfIdEscape(jsfId) {
	var filtered = jsfId.replace(/\\*:/g, "\\:");
	console.log(filtered);
	return filtered;
}

function jsfId(id) {
	return jsfIdEscape(id).replace(/^#?/, "#")
}
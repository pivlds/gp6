function test() {
	alert("Hello")
}

function readTable(selector) {
	var chart = document.querySelector(selector);

	console.log(chart);

	var options = chart.dataset;

	console.log(options);

	// now process table

	var table = document.getElementById(options.table);
	if (table == null) {
		console.log("ERROR: Couldn't locate table: " + options.table)
		return null;
	}

	var tbody = table.tBodies[0];
	var thead = table.tHead;
	var trows = tbody.rows;

	console.log(table);
	console.log(thead);
	console.log(tbody);

	var hrow = options.headerRow;

	var tcols = thead.rows[hrow].cells;
	var cols = [];

	// note the < entity because of jsf pre-processor
	for (var i = 0; i < tcols.length; i++) {
		var newVal = tcols[i].textContent.replace(/\s+/g, " ").trim();
		cols[cols.length] = newVal;
	}
	console.log(tcols);
	console.log(cols);

	var rows = [];
	for (var r = 0; r < trows.length; r++) {
		rows[rows.length] = rowToArray(trows[r]);
	}

	var r = {
		columns : cols,
		rows : rows,
		options : options
	};

	filterColumns(r, readColumns(options.columns));
	convertColumns(r, options.axisYType)

	// TODO: make this optional based on `data-reverse-x` attribute
	flipRows(r);

	console.log(r);
	return r;
}

/**
 * Reverses the order the `rows` array property
 * 
 * @param table
 *            An object with property `rows`
 */
function flipRows(table) {
	var rows = table.rows;
	var newRows = [];

	for (var r = rows.length - 1, nr = 0; nr < rows.length; nr++, r--) {
		newRows[nr] = rows[r];
	}

	table.rows = newRows;
}

function convertColumns(table, format) {
	if (typeof (format) === 'undefined')
		return;

	var parser = format == 'int' ? parseInt : parseFloat;

	for (var r = 0; r < table.rows.length; r++) {
		for (var c = 1; c < table.rows[r].length; c++)
			table.rows[r][c] = parser(table.rows[r][c]);
	}
}

function filterColumns(table, useCols) {
	for (var c = 1; c < table.columns.length; c++) {
		var tcol = table.columns[c];
		var found = false;
		for (var uc = 0; uc < useCols.length; uc++) {

			var ucol = useCols[uc];

			if (ucol == tcol) {
				found = true;
				break;
			}
		}

		if (!found) {
			table.columns.splice(c, 1)
			for (var r = 0; r < table.rows.length; r++) {
				table.rows[r].splice(c, 1);
			}
			c--;
		}
	}
}

function rowToArray(row) {
	var r = [];
	for (var i = 0; i < row.cells.length; i++) {
		r[r.length] = row.cells[i].textContent;
	}
	return r;
}

function readColumns(columnList) {
	var r = columnList.split(/[,]/);
	for (var i = 0; i < r.length; i++) {
		r[i] = r[i].trim();
	}
	return r;
}

window.chartsReady = false;
window.chartQueue = [];

function startCharts() {
	window.chartsReady = true
	for (chart in chartQueue) {
		drawChart(chartQueue[chart])
	}
}

function drawChart(chartSelector) {
	if (typeof (chartSelector.id) !== 'undefined')
		chartSelector = "#" + chartSelector.id;

	if (!chartsReady) {
		chartQueue[chartQueue.length] = chartSelector
		return;
	}

	console.log(chartSelector)
	chartSelector = jsfIdEscape(chartSelector);

	var target_div = document.querySelector(chartSelector);

	if (target_div == null) {
		console.log("Chart Selector could not be reified: " + chartSelector)
		return;
	}

	var tdata = readTable(chartSelector);
	if (tdata == null)
		return;

	var arr = [ tdata.columns ].concat(tdata.rows);

	var data = google.visualization.arrayToDataTable(arr);

	// Set chart options
	var options = {}
	options.title = tdata.options.title.replace(/\\n/g, "\n");
	options.hAxis = {}
	options.hAxis.title = tdata.options.axisX

	options.vAxis = {
		title : tdata.options.axisY,
		viewWindow : {}
	}

	if (tdata.options.minValue) {
		options.vAxis.viewWindow.min = tdata.options.minValue
	}
	if (tdata.options.maxValue) {
		options.vAxis.viewWindow.max = tdata.options.maxValue
	}
	options.legend = {
		position : "top"
	}

	var draw_div = target_div
	if (typeof (tdata.options.target) !== 'undefined') {
		draw_div = jsfIdEscape(tdata.options.target);
		draw_div = document.querySelector(draw_div);
	}

	if (typeof (draw_div.className) === 'undefined' || draw_div.className == "") {
		draw_div.className = 'chart';
	}

	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.LineChart(draw_div);
	chart.draw(data, options);
}

function ensureChartsLoader() {
	var loader = document.querySelector("#google-charts-loader");
	if (loader == null)
		loader = document.querySelector('script[src$="charts/loader.js"]');

	if (loader != null) {
		console.log("Google Charts Loader already present")
		console.log(loader)
	} else {
		var script = document.createElement("script")
		script.id = "google-charts-loader"
		script.type = "text/javascript"
		script.src = "https://www.gstatic.com/charts/loader.js"
		console.log("Adding Google Charts Loader to Head")
		document.head.appendChild(script)
		console.log(script)
	}
}

ensureChartsLoader()

window.onload = function() {
	google.charts.load('current', {
		'packages' : [ 'corechart', 'line' ]
	})
	google.charts.setOnLoadCallback(startCharts);
}

window.onresize = function() {
	console.log("RESIZE");
	var charts = document.querySelectorAll("[data-chart=true]")
	for (var i = 0; i < charts.length; i++) {
		drawChart(charts[i]);
	}
}